[Automation Management Summit April 2017](https://http://www.ams2017.com/) Trip Report

* St. Louis, MO
* April 20, 2017
* Mike Lombardi

## What is the Automation Management Summit

This is a smaller, local conference that focuses on the local IT and business community and specializes in automation and the emerging patterns and best practices for improving flow of work, reliability, and reducing cost.

## Why I chose to attend the Automation Management Summit

I wanted to attend AMS for the second time (I went last year, too!) because I _knew_ I would meet people in my **local** community who are adopting or wanting to adopt better practices and to expand their skillsets.
I wanted to be a resource for them and to learn from their experiences in turn.

I _also_ went to represent the [St. Louis PowerShell User Group](http://meetup.com/stlpsug) which [Model](http://model-technology.com/) had kindly donated a sponsorship to!
They gave us a room for a lunch presentation, space to talk with attendees, advertising, and helped us reach out to other folks in the community.
I cannot overstate how awesome this was for us as an all-volunteer group.

The final reason I attended (as if all of that wasn't enough!) was because the organizers had asked if I would be willing to [share my experience with infrastructure automation during the keynote interview](https://youtu.be/CSZ0CL-qh5M).
I was really looking forward to getting to hammer on how important the cultural side of the transformation is and how critical soft skills are to any successful team.

## Takeaways

+ I spend a _lot_ of my time living in the smaller DevOps bubble and I **need** to talk to more of the practitioners in the community
  + There's a growing hunger for change but so many of the concepts are still new or the learning curve is steep
  + People who know where the bear traps are (I've found a _few_) should help other folks avoid them.
+ Hybrid cloud is sticking around.
  + This echoes what I saw at both the PowerShell Summit and the DevOpsLibrary Nano Conference
+ Automation and the changes that are coming to every organization isn't avoidable
+ We need to be focusing on delivering value and services, not on settings or servers
+ There's a ton of room in our community for sharing and growth
+ There are people all over my town doing crazy good work
+ I have done a poor job leveraging my local community to this point - both for learning and support, but also as a target audience for making things better.

## Detailed Session Information

### Azure Stack 101 with [Nathan R](https://twitter.com/etc_passwd)

#### Summary
A talk about the current state and immediate future of the Azure Stack as well as scenarios and reasons to (and not to!) use Azure Stack.

#### Takeaways
+ GA is coming up soon
  + Mostly feature-parity with Azure, a few services will still be in preview at GA
+ A potential phenomenal use case for Azure Stack is places with limited bandwidth or which have to reside off the internet
  + Planes, ships, critical infrastructure locations?
+ Could be a useful tool for helping app/dev teams migrate their applications and architecture from legacy configuration models to more cloud-native ones.
+ Make "on prem or public cloud?" a runtime choice for your teams
+ Multi-cloud is going to require strong troubleshooting and engineering chops, even if tools are agnostic
+ Writing plugins for Azure makes it easy to use them in Azure Stack
+ Significant value might be around the portal experience _as well_ as all the technical benefits
  + [Changing our tools for the right reasons might help us effect the culture change we want](https://medium.com/@BillHiggins/tools-as-a-catalyst-for-culture-change-f012b2c0b527)
+ We'll be able to run the Azure WAF and LBs on prem via Azure Stack
+ Choosing between the consumption model and capacity model for pricing will be interesting
  + If you think adoption will be really low for a while, might make sense to go for consumption
  + If you're expecting to forklift existing workloads, look at capacity
    + But maybe don't forklift, it hasn't worked out well for anyone, really.
+ The value of leveraging cloud fabric concepts in your datacenter is resiliency.
  + What happens if I knock over a random rack in your DC?
+ Might be useable as a safe testing ground for reimplementing legacy apps into cloud patterns.
+ Expensive by comparison to Azure.
  + There's a [TCO Tool](http://tco.microsoft.com)
+ Leveraging quotas for subscriptions might make Azure Stack *perfect* for a lab
  + Might be a good idea to offer "free" subscriptions to teams internally as a way to encourage experimentation
+ Usable with Azure AD and MFA
+ Same tooling for management!
+ Subscriptions for app/dev teams or tenants for both Azure Stack and Azure will need to be maintained separately, for now
  + No "single pane of glass"
+ Minimal required administration of the under-the-hood stuff
  + Black boxy, but comes with guarantees and support

#### Action Items
+ [ ] Keep an eye on the upcoming blogs and discussions
  + Unfortunately, not much I can do directly since the cost of entry is too high for my homelab.

### [Alexa: Bring Me One Step Closer to Couch Administration]() with [Jesse Walter](https://twitter.com/jessccm)

#### Summary
This talk dove into some 101-level technical information around setting up an integration with Amazon's natural language interpretation + bot service, as well as practical use cases for it in the workplace.

#### Takeaways
+ We can use Alexa/Cortana as voice-driven automation for desktop environments.
  + For example, getting information about recent emails or slack alerts or _whatever_ on **my** schedule and without having to open up an app.
  + > Cortana, have I received any new high-priority emails today?
  + > Alexa, when's my next scheduled appointment?
+ Strong overlaps with ChatOps principles
  + Strongly prefer slack for our working team but provide cortana/alexa as a user interface for our services to our partners.
    + We should keep our team work in slack because that keeps the _context_ of our work all together - we don't lose out on the natural language processing though since it can read plaintext from our chat, too.
    + An example use case for our partners might be `"What's my Azure resource foot print right now?"` and getting that info back. Ditto for `"What're my projected expenditures across my SuperApp accounts?"`
+ Open/send emails, request statistics or monitoring alerts, etc.
+ Not too many use cases directly available for infrastructure automation but _boundless_ opportunities for providing business value back to our partners/customers.
  + They should be able to file tickets, create basic feature requests, request help, check their expenses, get info on their services, review outages or news feeds from us, etc.
+ If possible, make your bots multilingual - meet your partners where they're most comfortable.
+ Potential use case is when infrastructure teams are meeting face-to-face and we need to quickly retrieve data.
  + For example, in a meeting about firewall change processes: `'Alexa, how many firewall requests were filed in the last day, the last week, and the last month?'`
+ Use for workflows such as requesting or submitting approvals, scheduling meetings or changes, etc.
+ For our team, any actions we take via Alexa/Cortana should _also_ post information to Slack
  + We want to keep deriving value from our contexts!
+ Role based access might be a concern - may have to build into our resources
  + Voice-Recognition is coming.
  Maybe a combination of voice-recognition and passcodes can work for this?
+ Auditing and logging isn't super well defined out of the box, but it should be easy to shovel into ELK or similar.
+ If we're doing anything related to sensitive data or infrastructure, privilege management has to be figured out.
+ There's a security concern around always-on-voice in the office.
+ Everything we get from Alexa/Cortana should **also** work in plaintext, so we might leverage this via a chat bot first (where we _can_ control access and priveleges).

#### Action Items
+ [ ] Investigate writing using the tools behind both Alexa and Cortana
+ [ ] Create a small, simple example integration where the bot is able to query a single node for its health statuses.
+ [ ] Evangelize internally
+ [ ] Research security options around chat bots like Cortana and Alexa in the workplace
  + [ ] Specifically see what can be done around privelage management.
  This is a roadblock to adoption.
+ [ ] Speak with Jesse about a couple of health monitor example use cases

### Hallway Track

#### Summary
As the STLPSUG organizer, I had a table at the conference to hang out at and meet folks from the community.
I also spent some time wandering the halls chatting with people and getting insight into what they were learning or working on.
I also gave a talk myself during lunch on running PowerShell scripts as services on Windows Servers.
Finally, I talked with folks as the summit wrapped up.

#### Takeaways
+ The community of people doing DevOps on Windows is pretty small in St. Louis, but getting bigger.
+ More and more organizations are adopting an automate-all-the-things mindset
+ Many organizations are still just getting their feet wet with advanced PowerShell concepts.
  + Most of them still haven't transitioned towards configuration management tooling but want to.
+ _Everyone_ is struggling with changing their culture and there's a growing interest in this.
+ There are numerous awesome resources for St. Louis professionals to reach out to, but getting the word out seems to be difficult.
  + Perhaps because of the lower density, many folks who might find help in one another just never know the other exists.
  + Might be a community communication issue.

#### Action Items
+ [ ] Attend more of the DevOps meetups here in STL.
+ [ ] Make sure to focus more on our local connections than I have in the last two years
+ [ ] Actively reach out to folks in the area and see what the community can be doing to help and connect.